<?php

/*
 * @file
 * Add the appropriate information here. 
 *
 * Description
 *
 * Variables
 *
 * return @form
 */

function emugepc_add_more($form, &$form_state, $no_js_use = FALSE) {
  
  if(isset($_SESSION['values']['distiributor_number']) || isset($_SESSION['values']['items'])) unset($_SESSION['values']); 
  unset($_SESSION['values']['']);
  
  // prevent this page from caching to prevent ajax issues
  $GLOBALS['conf']['cache'] = false;  
  
  // Status of the app message. Use error for down status.
  #drupal_set_message('Emuge Price Checker Status: Working and Current', 'status', FALSE);
  
  $form['description'] = array(
    '#markup' => '<hr><a href = "/parts-search">Click for the NEW Multipart Lookup ></a>',
    '#weight' => 70,
  );

  // Because we have many fields with the same values, we have to set
  // #tree to be able to access them.
  $form['#tree'] = TRUE;
  $form['parts_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enter Emuge Part Numbers'),
    
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="names-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  // Build the fieldset with the proper number of names. We'll use
  // $form_state['part_num'] to determine the number of textfields to build.
  if (empty($form_state['part_num'])) {
    $form_state['part_num'] = 1;
  }
  // On rebuild, we loop through the existing parts_fieldsets
  // and update accdordingly
  for ($i = 0; $i < $form_state['parts_fieldset'] + 1; $i++) {
    
    // Increment the fieldsets
    $form[$i . '_fieldset'] = array(
    '#type' => 'fieldset',
    );
    
    // Add this fieldsets part field
    $form['parts_fieldset'][$i]['part'][$i] = array(
      '#type' => 'textfield',
      '#title' => t('Part Number'),
      '#size' => 80,
      // add an autocomplete callback menu item
      '#autocomplete_path' => 'emugepc/autocomplete',

    );
    
    // Add this fieldsets quantity field
    $form['parts_fieldset'][$i]['quantity'][$i] = array(
      '#type' => 'select',
      '#title' => t('Quantity'),
      '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25)),
      '#default_value' => 1,
    );
    
  }
  // Button to add an additional part
  $form['add_part'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#weight' => 49,
    '#submit' => array('emugepc_add_more_add_one'),
    '#attributes' => array(
        'class' => array('boxed-button'),
     ),
    
    // See the examples in emugepc.module for more details on the
    // properties of #ajax.
    '#ajax' => array(
      'callback' => 'emugepc_add_more_callback',
      'wrapper' => 'names-fieldset-wrapper',
    ),
  );
  // Button to remove a row up to the last one
  $form['remove_part'] = array(
    '#type' => 'submit',
    '#value' => t('Remove'),
    '#submit' => array('emugepc_add_more_remove_one'),
    '#weight' => 50,
    '#attributes' => array(
        'class' => array('boxed-button')
     ),
    '#ajax' => array(
      'callback' => 'emugepc_add_more_callback',
      'wrapper' => 'names-fieldset-wrapper',
    ),
  );
  
  module_load_include('inc', 'ecreports', 'ecreports.salesview');
  // The particular account with which to apply a discount
  
  $form['distributor-account'] = array(
    '#type' => 'select',
    '#title' => t('Distributor'),
    #'#options' => emugepc_options_list(),
    '#options' => _ecreports_view_salesview_get_options(),
    '#weight' => -20,
    // Using the $_SESSION variable, set the selection of the 
    // distributor to the last submitted value
    '#default_value' => 
      isset($_SESSION['values']['distiributor_number']) ? 
        $_SESSION['values']['distiributor_number'] : '',
  );
    // Standard submit button with special class
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 51,
    '#attributes' => array(
        'class' => array('boxed-button')
     ),
  );

  // This simply allows us to demonstrate no-javascript use without
  // actually turning off javascript in the browser. Removing the #ajax
  // element turns off AJAX behaviors on that element and as a result
  // ajax.js doesn't get loaded.
  // For demonstration only! You don't need this.
  if ($no_js_use) {
    // Remove the #ajax from the above, so ajax.js won't be loaded.
    if (!empty($form['parts_fieldset']['remove_part']['#ajax'])) {
      unset($form['parts_fieldset']['remove_part']['#ajax']);
    }
    unset($form['parts_fieldset']['add_part']['#ajax']);
  }
  // Return the udpated and prepared form
  return $form;
}

function emugepc_stock_checker($product_id, $CustomerNumber) {
  
  
  # Declare and gather needed data for this function
  global $user;
  $account = user_load($user->uid);
  # List of Multitap prefixs. Used to determine correct pricing table. Added new items
  # on 5/26/15 rick
  /*
  $mt_ids = array (
    'B1577300', 'B4933200', 'B5991400', 
    'BU497300', 'BU533200', 'BU591410', 
    'C1577300', 'C4933200', 'CU497300', 
    'CU533200', 'BU499300', 'CU499300',
    'BU539300', 'CU539300', 'BU533200',
    'BU533200', 'B1579300', 'B1577300',
    'B4933200', 'B4939300', 'C1579300',
    'C4939300', 'CU031410',
    );
  */
  $mt_ids = array();
  // Grab the prefix of the provided number
  $prefix = substr($product_id, 0, 8);
  
  // Check prefix for in multitap array.
  $pricing_table = (in_array($prefix, $mt_ids)) ? 'pricing_mt': 'pricing_std';
  
  # clean up product_id to remove any '.'
  $product_id = preg_replace('/\./', '', $product_id);
  
  # Get product data from the corresponding table 
  db_set_active('edbd');
  
  
    
    $product = db_select($pricing_table, 'p')
      ->fields('p')
      ->condition('ITMNBR', $product_id, 'LIKE')
      ->execute()
      ->fetchObject();
  db_set_active(); 
       
  # Get customer information from state of CustomerNumber variable    
  if (!$CustomerNumber) {
    $items = field_get_items('user', $account, 'field_customer_id');
    $CustomerNumber = $items[0]['value'];
  }   
  
  $CustomerInfo = _emuge_customer_load($CustomerNumber);
  
  # Assign values to the product object where other methods expect
  if($product){
    $product->lstprc      = $product->LSTPRC;
    $product->qtyus       = $product->QTYUS;
    $product->qtyde       = $product->QTYDE;
    $product->product_id  = $product->ITMNBR;
    $product->name        = $product->ITMDESC;
    
    # Apply applicable discount
    $discountCode     = ($pricing_table == 'pricing_std') ? $CustomerInfo['DiscountCode'] : $CustomerInfo['MTcode'];
    $product->qtypkg  = ($product->{$discountCode . 'B'}) ? $product->{$discountCode . 'B'} : 1 ;
    $product->price   = ($product->{$discountCode . 'N'}) ? $product->{$discountCode . 'N'} : $product->LSTPRC;
  
    # Check for Elite Status  
    /* Removed on 2/29/2016 - RP
    if(is_elite($CustomerNumber)) {
    
      try {
      # Get discount based on product class
      $query= Database::getConnection('default', 'ct');
      $results = $query->query("SELECT * FROM elite_program_2016 WHERE tool_class = $product->ITMCLS")->fetchAssoc();
      
      } 
      catch(Exception $e) {
        # Handle errors
        drupal_set_message($e->getMessage(), 'error');
      }
      
      # Apply the Elite Program discount
      $product->price = $product->LSTPRC * (1 - ($results['level_five_discount'] * .01));
      
    } 
    else {
      # Apply the standard discount
      
      $product->price   = ($product->{$discountCode . 'N'}) ? $product->{$discountCode . 'N'} : $product->LSTPRC;
    }
    */
    
  }
  
  
   
  # Return product object
  return $product;   
  
}

function emugepc_results () {
  
  $items = $_POST['parts_fieldset'];
  $customer_number = $_POST['distributor-account'];
  $markup = '';
  $page = array();
  
  foreach($items as $item) {
    if(isset($item)) {
      // $i variable to keep track of the part fields and quantities
      if(!isset($i)) {
        $i = 0;
      }
      else {
        $i++;
      }
      // Gather this parts qantity
      $quantity = (int)$item['quantity'][$i];
      // Gather this parts stock info
      $part = (array)emugepc_stock_checker($item['part'][$i], $customer_number);
      
      // Add the quantity to the part array if there was an emugepc_stock_checker match
      if ($part) {
        
        $part['quantity'] = $quantity;
        
        $markup .= $part['product_id'] . ' is a correct part number.';
        
      } else {
        
        $markup .= $item['part'][$i] . ' is not a correct part number';  
      }
    }
   
  }
 
  return $page['#markup'] = $markup;
}

function emugepc_get_result ($form, &$form_state) {
    
  $CustomerNumber = $form_state['values']['distributor-account'];
  $title = 'Emuge Account #' . $CustomerNumber;
  
  $header = array(t('Party Number'), t('QTY'), t('Description'), t('List Price (ea)'), t('Net Price (ea)'), t('USA'), t('GER'), t('Package Qty'), t('Net Price'));
  $invalid  = array();
  $stock = '';
  $package = '';
  $rows = array();
  $total = 0;
  foreach ($form_state['values']['parts_fieldset'] as $prod) {
       
      $qty = (integer)$prod['quantiy'];
      $product = emugepc_stock_checker(strtoupper($prod['part'][0]), $CustomerNumber);
      if ($product) {
        $class = array();
        if ($qty != 0) {
          if ($qty > $product->qtyus + $product->qtyde) {
            $stock = '<div class="stock">PLEASE NOTE: Requested Stock not currently available.</div>';
            $class[] = 'stock';
          }
        } 
        else {
          $package = '<div class="package">PLEASE NOTE: Requested Stock only available in package quantities.</div>';
          $class[] = 'package';
        }
      
        // BULTLER BROTHERS clause per Bob Hellinger. He requested that they do not see a discount and instead see N/A
        $product->price = ($CustomerNumber == '8151') ? 'N/A' :  number_format($product->price, '2');
        // Use the list price for BUTLER BROTHERS items
        $price = (is_numeric($product->price)) ? $product->price : $product->lstprc;
       
        $rows[] = array(
          'data' => array(
            '<span class="_small">' . $product->product_id . '</span>',  '   ' . $qty,
            '<span class="small">' .  $product->name . '</span>', 
            '$' . number_format($product->lstprc, '2'), 
            '$' . $product->price, 
            '<div>' . $product->qtyus . '</div>', 
            '<div>' . (empty($product->qtyde) ? '0' : $product->qtyde) . '</div>', '<div>' . $product->qtypkg . '</div>', 
            '$' . number_format($price * $qty, 2),
          ),  
            'class' => $class,
        );
        
        $total +=  number_format($price * $qty, 2);
        
    } 
    else {
      $invalid[] = t('%pid is not a valid product number', array('%pid' => $prod['part']));
      
    }
  }
 
  $result = array();
  
  if ($total) {
    
   $result['pdf'] = array (
    '#theme' => 'link',
    '#path' => 'check-stock-price/' . $entityform_id . '/printpdf',
    '#text' => t('PDF'),
    
    '#description' => t('Send this page as a PDF.'),
    '#options' => array(
    'attributes' => array(
      'class' => array('orange-button', 'hide'),
      'target' => array('_blank'),
    )
      ),
        '#prefix' => '<div class="op">',
   ); 
    
   $result['email'] = array(
      '#theme' => 'link',
      '#path' => 'check_stock_price/' . $entityform_id . '/printmail',
      '#text' => t('Email List'),
      '#module' => 'print_mail',
      '#format' => 'mail',
      '#description' => t('Send this page by email.'),
        '#options' => array(
          'attributes' => array(
            'class' => array('orange-button', 'hide'),
            #'style' => array('display:none'),
          ),
        ),
       // '#prefix' => '<div class="op">',
      );
    
    $result['print'] = array(
        '#theme' => 'link',
        '#path' => 'check-stock-price/result/' . $entityform_id . '/print',
        '#text' => t('Print List'),
        '#options' => array(
          'attributes' => array(
            'class' => array('orange-button', 'hide'),
            'target' => array('_blank'),
          ),
        ),
        '#suffix' => '</div>',
    );
  }
  $result['list'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#prefix' => $title,
  );
  
  if ($total) {
   $result['total'] = array(
      '#markup' => '<div class="total" style="margin-top: 15px">Total: $' . number_format($total, '2') . '</div>',
    );
  }
  
  $result['invalid'] = array(
    '#theme' => 'item_list',
    '#items' => $invalid,
    '#prefix' => '<div class="invalid">',
    '#suffix' => '</div>',
  );
  
  $result['note'] = array(
    '#markup' => $package . $stock,
  );

  $result['back'] = array(
    '#theme' => 'link',
    '#path' => 'emugepc',
    '#text' => t('Back'),
    '#options' => array(
      'attributes' => array(
        'class' => array('orange-button', 'hide'),
                  'style' => array('margin-bottom:20px'),
      ),
    ),
  );
  
  $result['disc'] = array(
    '#theme' => 'link',
    '#text' => t('Stock and pricing snapshot updated twice daily, GER stock updated once daily. Subject to change.'),
        '#options' => array(
      'attributes' => array(
        'class' => array('nolink', 'hide'),
        'style' => array('font-size:11px; display:block; margin-bottom:20px; text-decoration: none;'),
      ),
    ),
  );

  return $result;
};

/**
 * Function 
 */
function emugepc_results_page($form_state) {
  
  $form['heading'] = array(
    '#type' => 'markup',
    '#markup' => 'This is the markup for the heading of the form. this is new markup for this header.',
    '#prefix' => '<div class="w100">',
    '#suffix' => '</div>',
  );
  
  $form['table'] = array(
    '#type' => 'markup',
    '#markup' => 'This is the markup for the table of the form',
    '#prefix' => '<div class="w100">',
    '#suffix' => '</div>',
  );
  
  $form['footer'] = array(
    '#type' => 'markup',
    '#markup' => 'This is the markup for the footer of the form',
    '#prefix' => '<div class="w100">',
    '#suffix' => '</div>',
  );
  
  $page['#markup'] = 'this is some markup';
  
  return $page;
}


function emugepc_add_more_add_one($form, &$form_state) {
  $form_state['parts_fieldset']++;
  $form_state['rebuild'] = TRUE;
}

function emugepc_add_more_remove_one($form, &$form_state) {
  if ($form_state['parts_fieldset'] >= 1) {
    $form_state['parts_fieldset']--;
  }
  $form_state['rebuild'] = TRUE;
}

function emugepc_add_more_callback($form, $form_state) {
  return $form['parts_fieldset'];
}

function emugepc_add_more_submit($form, &$form_state) {
  $output = 'Submitted';
  $_SESSION['values'] = array(
    'distiributor_number' => $form_state['values']['distributor-account'],
    'items' => $form_state['values']['parts_fieldset'],
  );
  
  drupal_goto('emugepc/results');
  
  return $form;
}

/**
 * Function to process [emugepc] form submissions. It gathers distributor, item, disocunts, 
 * and totals and displays a table of results.
 *
 * @return string $markup
 *  Markup to be rendered by drupal
 */
function emugepc_submit_page_display() {
    $distributor_number = ($_SESSION['values']['distiributor_number'] != '') ? $_SESSION['values']['distiributor_number'] : false;
    $parts = $_SESSION['values']['items'];
    $discount = '';
    $items = array();
    $order_total = 0;
    $errorValues = array();
    $markup = '';
    // Declare table column names
    $header = array(
      t('Part Number'), 
      t('Item Class'),
      t('QTY'), 
      t('Description'), 
      t('List Price'), 
      t('Discount'), 
      t('Net Price'), 
      t('USA'), 
      t('GER'), 
      t('Package Qty'), 
      t('Price')
      );
      
    // Get distributor information
    if($distributor_number) {
      $distributor = _emuge_customer_load($distributor_number);
      $disc = preg_replace('/DISC/', '', $distributor['DiscountCode']);
      $distributor_name = $distributor['CustomerName'];
      $discount = $distributor['DiscountCode'];
      if(is_elite($distributor_number)) {
        $distributor_name .= ' - Emuge Elite Distributor';  
      }
      
      $markup .= "<h3>For Emuge Customer $distributor_name (#$distributor_number) - Discount Code {$disc}</h3>";    
    }
    else {
        $discount = 'List';
    }
    // Search the database for each part
    foreach($parts as $key => $part) {
      
      // prepare the item numbers for sql      
      $this_part = strtoupper($part['part'][$key]);
      $this_part = preg_replace('/\.|\s/', '', $this_part);
      
      $this_qty = $part['quantity'][$key];
      
      $product_search = emugepc_stock_checker($this_part, $distributor_number);
      if($product_search) {
        
        $items[$key] = $product_search;
        $items[$key]->quantity = $this_qty; 
      } elseif(strtoupper($part['part'][$key] == '')) {
      }
      else {
        array_push($errorValues, $part['part'][$key]);
      }
    }
    // Add each item row to the table    
    foreach($items as $item) {
      
      // Set the discount percentabe value for display
      $discountP = $discount . 'P';
      $item->discPercent = ($discountP == 'ListP') ? 'List' : $item->$discountP . '%';  
      // Run the item through an exception function for any special behavior
      emugepc_distributor_exceptions($distributor, $item);
      // Assign the $item values to the row cells
      $rows[] = array(
        $item->ITMNBR,
        $item->ITMCLS,
        $item->quantity,
        $item->name,
        '$' . number_format($item->lstprc, 2),
        $item->discPercent,
        '$' . number_format($item->price, 2),
        $item->qtyus,
        $item->qtyde,
        $item->qtypkg,
        '$' . number_format($item->price * $item->quantity, 2),
      );
      
      $order_total += $item->price * $item->quantity;
    }
    // Add the final row to the table
    $rows[] = array('','','','','','','','','','<em>Extended Total Price</em>','$' . number_format($order_total, 2));
    // Generate table markup and add to $markup
    $markup .= theme('table', array('header' => $header, 'rows' => $rows));
    // Add action links
    $markup .= '<div class="results-buttons">
        <a href="/print/emugepc/results" class="boxed-button">print</a>
        <a href="/printmail/emugepc/results" class="boxed-button">email</a>
        <a href="/printpdf/emugepc/results" class="boxed-button">pdf</a>
        <a href="/emugepc" class="boxed-button">back</a>
      </div>';
    // Display a message for all values that could not be found  
    if(count($errorValues)) {
      $errors = implode(', ', $errorValues); 
      $s = (count($errorValues) > 1) ? 's' : '';
      drupal_set_message('Invalid Item' . $s . ': ' . $errors, 'warning' ); 
    }
    if(count($rows) == 1) {
        drupal_set_message('No Results', 'error' );
    } 
    // Return markup to be rendered
    return $page['#markup'] = $markup;
   
}

/**
 * Emuge Multiple Entry form
 */
function emugepc_form($form, &$form_state) {
 
  $form['multiple-part-numbers'] = array(
    '#type' => 'textarea', //you can find a list of available types in the form api
    '#title' => 'Paste or Type (separated by a space) Emuge Part Numbers',
    '#size' => 50,
    '#required' => TRUE, //make this field required 
  );
  
  $form['distributor-account'] = array(
       '#type' => 'select',
       '#title' => t('Select Account'),
       '#options' => emugepc_options_list(),
        '#weight' => 40,
       );
  
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Check Stock'),
    '#submit' => array('emugepc_form_submit'),
    '#weight' => 50,
  );
  
  return $form;
}

function emugepc_form_validate($form, &$form_state) {
}

function emugepc_form_submit($form, &$form_state) {
  drupal_set_message(t('The form has been submitted. Again'));
}

function emugepc_form_submit_($form, &$form_state) {
  drupal_set_message(t('The form has been submitted. Again and Again'));
  
  $header = array(
      t('Part Number'), 
      t('QTY'), t('Description'), 
      t('List Price (ea)'), t('Net Price (ea)'), 
      t('USA'), 
      t('GER'), 
      t('Package Qty'), 
      t('Net Price')
      );
  
  $invalid  = array();
  $stock = '';
  $package = '';
  $rows = array();
  $total = 0;
  $CustomerNumber = $form_state['values']['distributor-account'];
  
  $items = array($form_state['values']['part-number']);
  $field_items = preg_split('/\W+/', $form_state['values']['multiple-part-numbers']);
  
  foreach($field_items as $addItem) {
     array_push($items, $addItem);
  }
  
  foreach ($items as $item) {
      $qty = 1;
       if ($product = emugepc_stock_checker(strtoupper($item), $CustomerNumber)) {
         
         
      $class = array();
      if ($qty%$product->qtypkg == 0) {
        if ($qty > $product->qtyus + $product->qtyde) {
          $stock = '<div class="stock">PLEASE NOTE: Requested Stock not currently available.</div>';
          $class[] = 'stock';
        }
      } else {
        $package = '<div class="package">PLEASE NOTE: Requested Stock only available in package quantities.</div>';
        $class[] = 'package';
      }
      
      // BULTLER BROTHERS clause per Bob Hellinger. He requested that they do not see a discount and instead see N/A
       $product->price = ($CustomerNumber == '8151') ? 'N/A' :  number_format($product->price, '2');
      // Use the list price for BUTLER BROTHERS items
       $price = (is_numeric($product->price)) ? $product->price : $product->lstprc;
       
      $rows[] = array(
        'data' => array(
          '<span class="_small">' . $product->product_id . '</span>',  '   ' . $qty,
          '<span class="small">' .  $product->name . '</span>', 
          '$' . number_format($product->lstprc, '2'), 
          '$' . $product->price, 
          '<div>' . $product->qtyus . '</div>', 
          '<div>' . (empty($product->qtyde) ? '0' : $product->qtyde) . '</div>', '<div>' . $product->qtypkg . '</div>', 
          '$' . number_format($price * $qty, 2),
          ), 
            'class' => $class,
          );
        
      $total +=  number_format($price * $qty, 2);
    
       }
       else {
           $invalid[] = t('%pid is not a valid product number', array('%pid' => $item));  
       }
  }
}

function emugepc_options_list() {
  $options = array();
  global $user;
  $user_fields = user_load($user->uid);
  $customer_sort = array();
  $options = array();
   
   // Section for Territory Managers
  if(isset($user->roles[7])) {
    global $databases;
    // Add the default, no discount, Distributor 
    $options[201400] = 'Select Emuge Distributor';
    
    $tmnumbers = array();
    // Updated field name 05.04.15 
	  $tmnumbers = $user_fields->field_emuge_tmn['und'];
	  /*
	   * Added 8.21.14 to handle users with multiple TMs
	   */
    foreach($tmnumbers as $tmnum){
      $tmnum = $tmnum['value'];
      
      if ($tmnum != NULL){
        Database::setActiveConnection('ds');
                
          /* Added 7.16.14 to handle searching all possible 
           * TM fields for a possible match. 
           */
          $or = db_or()
            ->condition('TM1', $tmnum)
            ->condition('TM2', $tmnum)
            ->condition('TM3', $tmnum)
            ->condition('TM4', $tmnum)
            ->condition('TM5', $tmnum)
            ->condition('TM6', $tmnum)
            ->condition('TM7', $tmnum)
            ->condition('TM8', $tmnum)
            ->condition('TM9', $tmnum)
            ->condition('TM10', $tmnum);
                           
            $accounts = db_select('tm_d_intermediary', 't')
                ->fields('t', array('dist_number'))
                ->condition($or)
                ->execute()
                ->fetchAll();
             Database::setActiveConnection();
                 
            foreach($accounts as $acctNum){
                $num = $acctNum->dist_number;
                $customers = db_select('customer_data', 'c')
                ->fields('c', array('id', 'CustomerName', 'CustomerNumber'))
                ->condition('CustomerNumber', $num, '=')
                ->execute();
                    
                   foreach($customers as $customer){
                        $customer_sort[$customer->CustomerName] = 
                            array('CustomerName' => $customer->CustomerName,
                                  'id'=>$customer->id, 
                                  'CustomerNumber' => $customer->CustomerNumber,
                             );
                    }  // End of loop customers
            } // End of loop acconts
     }// End of loop tm_numbers
    }
  }
  // Section for Emuge Employees
  if(isset($user->roles[6])) {
        $options[201400] = 'Select Emuge Distributor';

        Database::setActiveConnection();
        $customers = db_select('customer_data', 'c')
            ->fields('c', array('id', 'CustomerName', 'CustomerNumber'))
            ->execute();
            
        foreach($customers as $customer) {
             $customer_sort[$customer->CustomerName] = 
                array('CustomerName' => $customer->CustomerName,
                      'id'=>$customer->id, 
                      'CustomerNumber' => $customer->CustomerNumber,
                      );
        } // End of Loop
  
  }  
  // Section for authenticated users (Distributors)
  if (count($user->roles) == 1) {
        $customers = db_query(
          'SELECT * FROM lyy_customer_data WHERE CustomerNumber = :number',
          array(':number' => $user_fields->field_distributor_id['und'][0])
        );
       foreach($customers as $customer) {
             $customer_sort[$customer->CustomerName] = 
                array('CustomerName' => $customer->CustomerName,
                      'id'=>$customer->id, 
                      'CustomerNumber' => $customer->CustomerNumber,
                      );
        } // End of Loop customers
  }
  
  sort($customer_sort);
  
  foreach($customer_sort as $customer){
    $options[$customer['CustomerNumber']] = $customer['CustomerName'] . 
      ' (' . $customer['CustomerNumber'] . ')';
  }

  
  return $options;
}